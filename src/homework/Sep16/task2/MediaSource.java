package homework.Sep16.task2;

public abstract class MediaSource {

    private int id;
    private String pathToFile;

    public MediaSource(int id, String pathToFile) {
        this.id = id;
        this.pathToFile = pathToFile;
    }

    public int getId() {
        return id;
    }

    public String getPathToFile() {
        return pathToFile;
    }

}
