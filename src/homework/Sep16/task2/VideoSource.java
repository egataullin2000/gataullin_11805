package homework.Sep16.task2;

public class VideoSource extends MediaSource {

    private int quality;

    public VideoSource(int id, String pathToFile, int quality) {
        super(id, pathToFile);
        this.quality = quality;
    }

    public int getQuality() {
        return quality;
    }
}
