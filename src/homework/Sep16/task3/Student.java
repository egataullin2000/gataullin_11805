package homework.Sep16.task3;

public class Student {

    private int group;
    private String name;
    private String city;
    private int year; // Год рождения(?)

    public Student(int group, String name, String city, int year) {
        this.group = group;
        this.name = name;
        this.city = city;
        this.year = year;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Student{" +
                "group=" + group +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", year=" + year +
                '}';
    }
}
