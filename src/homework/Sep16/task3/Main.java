package homework.Sep16.task3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

public class Main {

    private static final String FILE_NAME = "data.txt";

    public static void main(String[] args) {
        Universe kfu = new Universe();
        List<Student> students = kfu.getStudents();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        try (FileWriter writer = new FileWriter(FILE_NAME)) {
            for (Student student : students) {
                if (thisYear > student.getYear() + 20 && !student.getCity().equals("Москва")) {
                    writer.write(student.toString());
                    writer.write("\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
