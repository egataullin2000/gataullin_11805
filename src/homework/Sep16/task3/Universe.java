package homework.Sep16.task3;

import java.util.Arrays;
import java.util.List;

public class Universe {

    private List<Student> students;

    public Universe() {
        students = Arrays.asList(
                new Student(805, "Гатауллин Эдгар", "Набережные Челны", 2000),
                new Student(805, "Дмитриев Михаил", "Казань", 2000),
                new Student(805, "Шигабутдинов Марат", "Казань", 2000),
                new Student(807, "Иванов Иван", "Москва", 2000),
                new Student(809, "Кто-то", "Не Москва", 1995)
        );
    }

    public List<Student> getStudents() {
        return students;
    }

}
