package homework.Sep16;

import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.close();
        double sum = 0, temp = 1;
        for (int k = 1; k <= n; k++) {
            temp *= 2d / k;
            sum += temp;
        }
        System.out.println(sum);
    }

}
