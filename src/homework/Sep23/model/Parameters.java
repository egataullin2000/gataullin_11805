package homework.Sep23.model;

public class Parameters {
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String ID = "id";
    public static final String FRIENDS = "friends";
    public static final String REMEMBER = "remember";
}