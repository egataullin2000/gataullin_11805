package homework.Sep23.model;

public class Pages {
    public static final String LOGIN = "/login";
    public static final String PROFILE = "/profile";
    public static final String MESSAGES = "/messages";
    public static final String INCOMING_MESSAGES = "/im";
    public static final String OUTCOMING_MESSAGES = "/om";
}
