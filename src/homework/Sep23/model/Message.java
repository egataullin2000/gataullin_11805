package homework.Sep23.model;

import java.util.Calendar;

public class Message {
    private User sender, receiver;
    private String text;
    private Calendar date;

    public Message(User sender, User receiver, String text, Calendar date) {
        this.sender = sender;
        this.receiver = receiver;
        this.text = text;
        this.date = date;
    }

    public User getSender() {
        return sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public String getText() {
        return text;
    }

    public Calendar getDate() {
        return date;
    }
}
