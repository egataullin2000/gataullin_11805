package homework.Sep23.servlet;

import homework.Sep23.model.*;
import homework.Sep23.render.LoginRender;
import homework.Sep23.service.UsersService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends Servlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (hasSession(request)) {
            response.sendRedirect(Pages.PROFILE);
        } else if (tryAuthWithCookies(request) || tryAuthWithForm(request, response)) {
            response.sendRedirect(Pages.PROFILE);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (hasSession(request)) {
            response.sendRedirect(Pages.PROFILE);
        } else {
            response.setContentType("text/html");
            PrintWriter writer = response.getWriter();
            writer.write(rendHtml(request));
            writer.close();
        }
    }

    @Override
    protected String rendHtml(HttpServletRequest request) {
        return new LoginRender().rend();
    }

    private boolean tryAuthWithCookies(HttpServletRequest request) {
        for (Cookie cookie : request.getCookies()) {
            if (CookieAttributes.USER_ID.equals(cookie.getName())) {
                request.getSession().setAttribute(SessionAttributes.CURRENT_USER_ID, cookie.getValue());
                return true;
            }
        }

        return false;
    }

    private boolean tryAuthWithForm(HttpServletRequest request, HttpServletResponse response) {
        checkParameters(request, Parameters.LOGIN, Parameters.PASSWORD);
        String login = request.getParameter(Parameters.LOGIN);
        String password = request.getParameter(Parameters.PASSWORD);
        User user = UsersService.getUserWith(login, password);
        if (user != null) {
            request.getSession().setAttribute(SessionAttributes.CURRENT_USER_ID, user.getId());
            checkParameters(request, Parameters.REMEMBER);
            boolean remember = Boolean.parseBoolean(request.getParameter(Parameters.REMEMBER));
            if (remember) {
                response.addCookie(getCookie(CookieAttributes.USER_ID, user.getId()));
            }

            return true;
        }

        return false;
    }
}