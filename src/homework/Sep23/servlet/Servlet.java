package homework.Sep23.servlet;

import homework.Sep23.model.SessionAttributes;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

public abstract class Servlet extends HttpServlet {
    protected void checkParameters(ServletRequest request, String... necessaryParameters) {
        for (String parameter : necessaryParameters) {
            if (request.getParameter(parameter) == null) {
                throw new IllegalArgumentException("Not all necessary parameters have been passed");
            }
        }
    }

    protected boolean hasSession(HttpServletRequest request) {
        return request.getSession().getAttribute(SessionAttributes.CURRENT_USER_ID) != null;
    }

    protected abstract String rendHtml(HttpServletRequest request);

    protected Cookie getCookie(Object key, Object value) {
        return new Cookie(key.toString(), value.toString());
    }
}
