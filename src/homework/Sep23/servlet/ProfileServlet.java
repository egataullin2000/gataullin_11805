package homework.Sep23.servlet;

import homework.Sep23.model.Parameters;
import homework.Sep23.model.SessionAttributes;
import homework.Sep23.model.User;
import homework.Sep23.render.ProfileRender;
import homework.Sep23.service.FriendsService;
import homework.Sep23.service.UsersService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

public class ProfileServlet extends Servlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        response.setContentType("text/html");
        writer.write(rendHtml(request));
        writer.close();
    }

    @Override
    protected String rendHtml(HttpServletRequest request) {
        int id = Integer.parseInt((String) request.getSession().getAttribute(SessionAttributes.CURRENT_USER_ID));
        User user = UsersService.getUserById(id);
        boolean showFriends = Boolean.parseBoolean(request.getParameter(Parameters.FRIENDS));
        List<User> friends = showFriends ? FriendsService.getFriends(id) : Collections.emptyList();

        return new ProfileRender(user, friends)
                .rend();
    }
}
