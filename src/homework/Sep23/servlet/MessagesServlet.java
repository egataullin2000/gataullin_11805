package homework.Sep23.servlet;

import homework.Sep23.model.*;
import homework.Sep23.render.MessagesRender;
import homework.Sep23.service.MessagesService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class MessagesServlet extends Servlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        if (hasSession(request)) {
            response.setContentType("text/html");
            writer.write(rendHtml(request));
            writer.close();
        } else response.sendRedirect(Pages.LOGIN);
    }

    @Override
    protected String rendHtml(HttpServletRequest request) {
        MessagesType messagesType = getMessagesType(request.getRequestURL().toString());
        List<Message> messages = null;
        int userId = (int) request.getSession().getAttribute(SessionAttributes.CURRENT_USER_ID);
        if (messagesType == MessagesType.INCOMING || messagesType == MessagesType.OUTCOMING) {
            messages = MessagesService.getMessages(userId, messagesType);
        } else if (messagesType == MessagesType.DIALOG) {
            checkParameters(request, Parameters.ID);
            int chatId = Integer.parseInt(request.getParameter(Parameters.ID));
            messages = MessagesService.getMessages(userId, chatId);
        }

        return new MessagesRender(messages, messagesType)
                .rend();
    }

    private MessagesType getMessagesType(String requestUrl) {
        if (requestUrl.contains(Pages.MESSAGES)) return MessagesType.DIALOG;
        if (requestUrl.contains(Pages.INCOMING_MESSAGES)) return MessagesType.INCOMING;
        if (requestUrl.contains(Pages.OUTCOMING_MESSAGES)) return MessagesType.OUTCOMING;

        return null;
    }
}
