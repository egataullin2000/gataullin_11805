package homework.Sep23.servlet;

import homework.Sep23.model.Pages;
import homework.Sep23.model.SessionAttributes;
import homework.Sep23.model.User;
import homework.Sep23.render.FriendsRender;
import homework.Sep23.service.FriendsService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FriendsServlet extends Servlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        response.setContentType("text/html");
        if (hasSession(request)) {
            writer.write(rendHtml(request));
            writer.close();
        } else response.sendRedirect(Pages.LOGIN);
    }

    @Override
    protected String rendHtml(HttpServletRequest request) {
        int id = (int) request.getSession().getAttribute(SessionAttributes.CURRENT_USER_ID);
        List<User> friends = FriendsService.getFriends(id);

        return new FriendsRender(friends)
                .rend();
    }
}
