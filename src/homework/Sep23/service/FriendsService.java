package homework.Sep23.service;


import homework.Sep23.dao.SubscriptionsDao;
import homework.Sep23.model.User;

import java.io.FileNotFoundException;
import java.util.List;

public class FriendsService {

    public static List<User> getFriends(int id) {
        List<User> friends = null;
        try {
            SubscriptionsDao subscriptionsDao = new SubscriptionsDao();
            friends = subscriptionsDao.getFriendsFor(id);
            subscriptionsDao.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return friends;
    }

}
