package homework.Sep23.service;

import homework.Sep23.dao.UsersDao;
import homework.Sep23.model.User;

import java.io.FileNotFoundException;

public class UsersService {

    public static User getUserById(int id) {
        User user = null;
        try {
            UsersDao usersDao = new UsersDao();
            user = usersDao.getUserById(id);
            usersDao.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return user;
    }

    public static User getUserWith(String login, String password) {
        User user = null;
        try {
            UsersDao usersDao = new UsersDao();
            user = usersDao.getUserWith(login, password);
            usersDao.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return user;
    }

}