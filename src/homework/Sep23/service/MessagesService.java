package homework.Sep23.service;

import homework.Sep23.dao.MessagesDao;
import homework.Sep23.model.Message;
import homework.Sep23.model.MessagesType;

import java.io.FileNotFoundException;
import java.util.List;

public class MessagesService {
    public static List<Message> getMessages(int id, MessagesType messagesType) {
        List<Message> messages = null;
        try {
            MessagesDao messagesDao = new MessagesDao();
            messages = messagesDao.getMessages(id, messagesType);
            messagesDao.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return messages;
    }

    public static List<Message> getMessages(int id1, int id2) {
        List<Message> messages = null;
        try {
            MessagesDao messagesDao = new MessagesDao();
            messages = messagesDao.getMessages(id1, id2);
            messagesDao.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return messages;
    }
}
