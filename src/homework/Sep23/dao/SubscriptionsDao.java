package homework.Sep23.dao;

import homework.Sep23.model.User;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SubscriptionsDao extends Dao {
    private static final String SRC = "C:\\Users\\ruegp\\Development\\Projects\\gataullin_11805\\subscriptions.txt";

    public SubscriptionsDao() throws FileNotFoundException {
        super(SRC);
    }

    public List<User> getFriendsFor(int id) throws FileNotFoundException {
        List<User> friends = new ArrayList<>();
        Set<Integer> userSubscribedOn = new HashSet<>();
        Set<Integer> subscribedOnUser = new HashSet<>();
        while (data.hasNext()) {
            int subscriber = data.nextInt();
            int subscribedOn = data.nextInt();
            if (subscriber == id) {
                userSubscribedOn.add(subscribedOn);
            } else if (subscribedOn == id) {
                subscribedOnUser.add(subscriber);
            }
        }
        UsersDao usersDao = new UsersDao();
        for (int friendId : userSubscribedOn) {
            if (subscribedOnUser.contains(friendId)) {
                friends.add(usersDao.getUserById(friendId));
            }
        }

        return friends;
    }
}