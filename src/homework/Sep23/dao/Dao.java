package homework.Sep23.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public abstract class Dao {
    protected Scanner data;

    protected Dao(String src) throws FileNotFoundException {
        data = new Scanner(new FileReader(src));
    }

    public void close() {
        data.close();
    }
}
