package homework.Sep23.dao;

import homework.Sep23.model.Message;
import homework.Sep23.model.MessagesType;
import homework.Sep23.model.User;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MessagesDao extends Dao {
    private static final String FILE_NAME = "C:\\Users\\ruegp\\Development\\Projects\\gataullin_11805\\messages.txt";

    public MessagesDao() throws FileNotFoundException {
        super(FILE_NAME);
    }

    public List<Message> getMessages(int id, MessagesType messagesType) {
        List<Message> messages = new ArrayList<>();
        while (data.hasNext()) {
            String[] msgData = data.nextLine().split(";");
            int senderId = Integer.parseInt(msgData[0]);
            int receiverId = Integer.parseInt(msgData[1]);
            boolean isIncomingReceivedByUser = messagesType == MessagesType.INCOMING && receiverId != id;
            boolean isOutcomingFromUser = messagesType == MessagesType.OUTCOMING && senderId != id;
            if (isIncomingReceivedByUser || isOutcomingFromUser) continue;
            messages.add(buildMessage(msgData));
        }

        return messages;
    }

    private Message buildMessage(String[] msgData) {
        User sender = null;
        User receiver = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
        Calendar date = null;
        try {
            UsersDao usersDao = new UsersDao();
            List<User> users = usersDao.getUsersById(
                    Arrays.asList(Integer.parseInt(msgData[0]), Integer.parseInt(msgData[1]))
            );
            usersDao.close();
            sender = users.get(0);
            receiver = users.get(1);
            date = Calendar.getInstance();
            date.setTime(dateFormat.parse(msgData[3]));
        } catch (FileNotFoundException | ParseException e) {
            e.printStackTrace();
        }

        return new Message(
                sender,
                receiver,
                msgData[2],
                date
        );
    }

    public List<Message> getMessages(int id1, int id2) {
        List<Message> messages = new ArrayList<>();
        while (data.hasNext()) {
            String[] msgData = data.nextLine().split(";");
            int senderId = Integer.parseInt(msgData[0]);
            int receiverId = Integer.parseInt(msgData[1]);
            if (senderId != id1 && senderId != id2 || receiverId != id1 && receiverId != id2) continue;

            messages.add(buildMessage(msgData));
        }

        return messages;
    }
}