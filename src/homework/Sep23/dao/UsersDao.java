package homework.Sep23.dao;

import homework.Sep23.model.User;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class UsersDao extends Dao {
    private static final String SRC = "C:\\Users\\ruegp\\Development\\Projects\\gataullin_11805\\users.txt";

    public UsersDao() throws FileNotFoundException {
        super(SRC);
    }

    public User getUserById(int id) {
        while (data.hasNext()) {
            if (data.nextInt() != id) {
                data.nextLine();
                continue;
            }
            data.next();
            data.next();
            return new User(id, data.next(), data.next());
        }

        return null;
    }

    public User getUserWith(String login, String password) {
        while (data.hasNext()) {
            int id = data.nextInt();
            if (!login.equals(data.next())) {
                data.nextLine();
                continue;
            }
            if (password.equals(data.next())) {
                return new User(id, data.next(), data.next());
            }
        }

        return null;
    }

    public List<User> getUsersById(Iterable<Integer> idList) {
        List<User> users = new ArrayList<>();
        for (int id : idList) users.add(getUserById(id));

        return users;
    }
}
