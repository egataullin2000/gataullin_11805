package homework.Sep23.render;

import homework.Sep23.model.User;

import java.util.List;

public class ProfileRender implements Render {
    private User user;
    private List<User> friends;

    public ProfileRender(User user, List<User> friends) {
        this.user = user;
        this.friends = friends;
    }

    @Override
    public String rend() {
        String html = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>" + user.getFullName() + "</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div>\n" +
                "    <h1>" + user.getFullName() + "</h1>\n" +
                "    <h3>ID: " + user.getId() + "</h3>\n";
        if (!friends.isEmpty()) {
            html += "    <h3>Friends: " + buildFriendsRow() + "</h3>\n";
        }
        html += "</div>\n" +
                "</body>\n" +
                "</html>";

        return html;
    }

    private String buildFriendsRow() {
        StringBuilder html = new StringBuilder(friends.get(0).getFullName());
        for (int i = 1; i < friends.size(); i++) {
            html.append(", ").append(friends.get(i).getFullName());
        }

        return html.toString();
    }
}
