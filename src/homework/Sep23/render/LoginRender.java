package homework.Sep23.render;

public class LoginRender implements Render {
    @Override
    public String rend() {
        return "<!DOCTYPE html>\n" +
                "<html lang=\\\"en\\\">\n" +
                "<head>\n" +
                "    <meta charset=\\\"UTF-8\\\">\n" +
                "    <title>Login</title>\n" +
                "    </head>\n" +
                "<body>\n" +
                "<form method=\"post\">\n" +
                "    <input type=\"text\" name=\"login\">\n" +
                "    <br>\n" +
                "    <input type=\"password\" name=\"password\">\n" +
                "    <br>\n" +
                "    <input type=\"checkbox\" name=\"remember\">Remember me</input>\n" +
                "    <br>\n" +
                "    <input type=\"submit\" value=\"Enter\">\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>";
    }
}
