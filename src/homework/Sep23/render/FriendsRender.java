package homework.Sep23.render;

import homework.Sep23.model.User;

import java.util.List;

public class FriendsRender implements Render {
    private List<User> friends;

    public FriendsRender(List<User> friends) {
        this.friends = friends;
    }

    @Override
    public String rend() {
        StringBuilder html = new StringBuilder("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Friends</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div>\n");
        for (User friend : friends) {
            html.append("\t").append(buildFriendRow(friend));
        }
        html.append("</div>\n" + "</body>\n" + "</html>");

        return html.toString();
    }

    private String buildFriendRow(User friend) {
        return "<h3>" + friend.getFullName() + "</h3>\n";
    }
}
