package classwork.Nov07;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;

    public static void main(String[] args) {
        System.out.println("Enter server's IP-address");
        Scanner scanner = new Scanner(System.in);
        String ip = scanner.next();
        System.out.println("Enter port");
        int port = scanner.nextInt();

        Client client = new Client();
        client.run(ip, port);
    }

    public void run(String serverIP, int serverPort) {
        Scanner scanner = new Scanner(System.in);
        startConnection(serverIP, serverPort);
        while (true) {
            String message = scanner.nextLine();
            sendMessage(message);
        }
    }

    private void startConnection(String ip, int port) {
        try {
            socket = new Socket(ip, port);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            new Thread(messageReceiverTask).start();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public void sendMessage(String message) {
        out.println(message);
    }

    private Runnable messageReceiverTask = new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    String response = in.readLine();
                    if (!"Exit".equals(response)) {
                        System.out.println(response);
                    } else {
                        stopConnection();
                        break;
                    }
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    };

    public void stopConnection() {
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
